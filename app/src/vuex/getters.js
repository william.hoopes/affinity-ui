export const mainCounter = state => state.counters.main
export const selected = state => state.selected.selected
export const lastSavePath = state => state.application.lastSavePath
export const project = state => state.affinity.project
export const affinity = state => state.affinity.project
export const typeNames = state => state.affinity.typeNames
