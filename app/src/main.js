import Vue from 'vue'
import Electron from 'vue-electron'
import Resource from 'vue-resource'
import Router from 'vue-router'
import ElementUI from 'element-ui'

import App from './App'
import routes from './routes'

// VueSplitter: https://github.com/rmp135/vue-splitter/blob/master/README.md
import VueSplitter from "@rmp135/vue-splitter"

//import 'jquery/dist/jquery.min.js'
//import 'bootstrap/dist/css/bootstrap.min.css'
//import 'bootstrap/dist/js/bootstrap.min.js'

// Icons:
//  vue-awesome: http://fontawesome.io/icons/
//  material design: https://materialdesignicons.com/
// [TODO]: Only import icons that we're using to reduce footprint
import 'vue-awesome/icons'
import Icon from 'vue-awesome/components/Icon.vue'
Vue.component('icon', Icon)

// Material Design Icons
// import 'icons/styles.css'
// import File from 'icons/file'
// import FileMultiple from 'icons/file-multiple'
// import Folder from 'icons/folder'
// import FolderOpen from 'icons/folder-open'

// Vue.component('model', File)
// Vue.component('instance', FileMultiple)
// Vue.component('namespace', Folder)
// Vue.component('namespace-open', FolderOpen)


import 'element-ui/lib/theme-chalk/index.css'
import 'element-ui/lib/index.js'


Vue.use(Electron)
Vue.use(Resource)
Vue.use(Router)
Vue.use(ElementUI)
Vue.config.debug = true

const router = new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes
})

Vue.directive('focus', {
  inserted: function (el) {
    el.focus()
  }
})

/* eslint-disable no-new */
new Vue({
  router,
  ...App
}).$mount('#app')
