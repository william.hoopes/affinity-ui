'use strict'

const electron = require('electron')
const path = require('path')
const $ = require('jquery')
const fs = require('fs-extra')
const app = electron.app
const Menu = electron.Menu
const dialog = electron.dialog
const ipcMain = electron.ipcMain
const BrowserWindow = electron.BrowserWindow
const events = require('./src/services/events.js')
const settings = require('./src/services/settings.js')

let mainWindow
let config = {}

if (process.env.NODE_ENV === 'development') {
   config = require('../config')
   config.url = `http://localhost:${config.port}`
} else {
   config.devtron = false
   config.url = `file://${__dirname}/dist/index.html`
}

let AppState = {
   lastProjectPath: settings.get('lastProjectPath', '')
}

function createWindow() {
   /**
    * Initial window options
    */
   mainWindow = new BrowserWindow({
      height: 600,
      width: 800,
      // backgroundColor: '#282828',
      darkTheme: true
   })

   mainWindow.loadURL(config.url)

   if (process.env.NODE_ENV === 'development') {
      BrowserWindow.addDevToolsExtension(path.join(__dirname, '../node_modules/devtron'))

      let installExtension = require('electron-devtools-installer')

      installExtension.default(installExtension.VUEJS_DEVTOOLS)
         .then((name) => mainWindow.webContents.openDevTools())
         .catch((err) => console.log('An error occurred: ', err))
   }

   mainWindow.on('closed', () => {
      mainWindow = null
   })

   mainWindow.$ = $
}

function createMainMenu() {
   const template = [
   {
      label: 'Project',
      submenu: [
         {
            label: 'Open...',
            click() {
               dialog.showOpenDialog({
                  title: 'Open Project',
                  properties: ['openDirectory'],
                  defaultPath: AppState.lastProjectPath || ''
               }, dirPath => {
                  if(mainWindow) {
                     mainWindow.webContents.send('project.open', dirPath[0])
                  }
               })
            }
         },
         {
            label: 'Save As...',
            click() {
               // Note:
               //    The showSaveDialog does not support selecting a directory.
               //    A showOpenDialog is used instead
               dialog.showOpenDialog({
                  title: 'Save Project',
                  properties: ['openDirectory', 'createDirectory', 'showHiddenFiles', 'promptToCreate'],
                  buttonLabel: 'Save Project',
                  defaultPath: AppState.lastProjectPath || ''
               }, dirPath => {
                  console.log(`Save As Path: ${dirPath}`)
                  if(mainWindow) {
                     if(Array.isArray(dirPath)) {
                        dirPath = dirPath[0] || ''
                     }
                     fs.mkdirpSync(dirPath)
                     mainWindow.webContents.send('project.save', dirPath)
                  }
               })
            }
         },
         {
            label: "Populate",
            click() {
               mainWindow.webContents.send('project.populate')
            }
         },
         { role: 'undo' },
         { role: 'redo' },
         { type: 'separator' },
         { role: 'cut' },
         { role: 'copy' },
         { role: 'paste' },
         { role: 'delete' },
         { role: 'selectall' }
      ]
   },
   {
      label: 'View',
      submenu: [
         { role: 'reload' },
         { role: 'toggledevtools' },
         { type: 'separator' },
         { role: 'togglefullscreen' }
      ]
   },
   {
      role: 'window',
      submenu: [
         { role: 'minimize' },
         { role: 'close' }
      ]
   },
   {
      role: 'help',
      submenu: [
         {
         label: 'Learn More',
         click () { require('electron').shell.openExternal('http://electron.atom.io') }
         }
      ]
   }
   ]

   if (process.platform === 'darwin') {
   template.unshift({
      label: app.getName(),
      submenu: [
         {
         role: 'about'
         },
         {
         type: 'separator'
         },
         {
         role: 'services',
         submenu: []
         },
         {
         type: 'separator'
         },
         {
         role: 'hide'
         },
         {
         role: 'hideothers'
         },
         {
         role: 'unhide'
         },
         {
         type: 'separator'
         },
         {
         role: 'quit'
         }
      ]
   })
   // Edit menu.
   template[1].submenu.push(
      {
         type: 'separator'
      },
      {
         label: 'Speech',
         submenu: [
         {
            role: 'startspeaking'
         },
         {
            role: 'stopspeaking'
         }
         ]
      }
   )
   // Window menu.
   template[3].submenu = [
      {
         label: 'Close',
         accelerator: 'CmdOrCtrl+W',
         role: 'close'
      },
      {
         label: 'Minimize',
         accelerator: 'CmdOrCtrl+M',
         role: 'minimize'
      },
      {
         label: 'Zoom',
         role: 'zoom'
      },
      {
         type: 'separator'
      },
      {
         label: 'Bring All to Front',
         role: 'front'
      }
   ]
   }


   let menu = Menu.buildFromTemplate(template)
   Menu.setApplicationMenu(menu)
}

app.on('ready', () => {
   createWindow()
   createMainMenu()
})

app.on('window-all-closed', () => {
   if (process.platform !== 'darwin') {
      app.quit()
   }
})

app.on('activate', () => {
   if (mainWindow === null) {
      createWindow()
   }
})